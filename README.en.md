# NodeJs basic server

## Introduction
- This is a NodeJs fully asynchronous basic server

## Software Architecture
- The overall code supports Promise, async and await asynchronous writing
- mysql service Use mysql2 library, faster than mysql
- http service uses koa library and encapsulates middleware and parsing get and post request parameters, as well as the encapsulation of asynchronous return
- log service uses log4js library

## Installation tutorial

1. Download the library file to the root directory and execute the command npm install
2. Modify the corresponding service configuration file (basically no need to modify, except for mysql configuration)
3. Start the server execute the command npm run server
4. After successful startup, you can visit [Server Home Page](http://127.0.0.1:2012/home) in the browser to check whether it is turned on normally (default: http://127.0.0.1:2012/home)