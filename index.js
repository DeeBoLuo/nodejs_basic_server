const server = require('./server/server');
const copy = require('./server/copy');

async function main() {
    global.ICPC = new server();
    return await null;
}

main().then(() => {
    console.log(copy[0]);
    console.log('程序运行中……');
}).catch((error) => {
    console.error('程序运行出错', error);
    throw error;
});

process.on('uncaughtException', (err, origin) => {
    console.error(`捕获的异常: ${err}`);
    console.error(`异常的来源: ${origin}`);
});