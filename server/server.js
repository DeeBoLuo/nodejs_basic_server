const log_service = require('./services/log_service/service');
const mysql_service = require('./services/mysql_service/service');
const web_service = require('./services/web_service/service');

module.exports = class {
    constructor() {
        let t = Date.now();
        console.log('服务器启动中……');
        console.log('------------------------------------------');
        //
        console.log('正在初始化日志服务');
        this.m_logService = new log_service();
        console.log('日志服务初始化完毕');
        //
        console.log('正在初始化数据库服务');
        this.m_mysqlService = new mysql_service();
        console.log('数据库服务初始化完毕');
        //
        console.log('正在初始化WEB服务');
        this.m_webService = new web_service();
        console.log('WEB服务初始化完毕');
        //
        console.log('------------------------------------------');
        console.log('服务器启动完毕', Date.now() - t, '毫秒');
    }
}