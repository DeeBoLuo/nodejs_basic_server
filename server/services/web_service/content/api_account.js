//
const {returnBody} = require('../web_function');
const {GET, POST} = require('./config');

module.exports = function (router) {
    //get 用 ctx.query post 用 ctx.request.body
    //主页
    router.get(GET.home, async ctx => {
        returnBody(ctx, 0, '', null, 'GET主页');
    });

    router.post(POST.home, async ctx => {
        returnBody(ctx, 0, 'ok', {print:'POST主页'});
    });
};
