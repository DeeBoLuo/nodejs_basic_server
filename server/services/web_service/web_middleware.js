const {returnBody} = require('./web_function');

module.exports = async function (ctx, next) {
    //ignore favicon
    if (ctx.path === '/favicon.ico') return;
    //条件
    if (ctx.request.url.includes('/login') || ctx.request.url.includes('/home')) {
        await next();
    } else {
        let str = ctx.request.url;
        console.debug(ctx.req.method, '响应', str);
        // let {username, token} = ctx.request.headers;
        // let [rows] = await db.getInstance().getToken(username, token);
        // if (rows == null || rows.length == 0) {
        //     returnBody(ctx, 10001, 'token已过期');
        //     console.log('token过期 请重新登录');
        //     return;
        // }
        let d = Date.now();
        await next();
        console.debug(ctx.req.method, '响应', str, '耗时:', Date.now() - d, '毫秒');
    }
};
