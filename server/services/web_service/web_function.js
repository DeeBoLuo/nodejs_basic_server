const TOKEN = `QWERTYUIOPASDFGHJKLZXCVBNM1234567890`;

module.exports = {
    returnBody(ctx, errCode, errorMsg, data, bodyText = null) {
        if (bodyText) {
            ctx.body = bodyText;
        } else {
            ctx.body = {
                errCode,
                errMessage: errorMsg,
                data,
            };
        }
    },

    getRandToken(tokenLength = 32) {
        let s = '';
        for (let i = 0; i < tokenLength; i++) {
            let len = Math.floor(Math.random() * TOKEN.length);
            s += TOKEN[len];
        }
        return s;
    }
}