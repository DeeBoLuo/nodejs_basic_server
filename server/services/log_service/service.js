const log4js = require('log4js');
const log4jsConfig = require('./config');

module.exports = class {
    constructor(category = 'default') {
        log4js.configure(log4jsConfig);
        const logger = log4js.getLogger(category);
        log4jsConfig.consoleLogChange(logger);
    }
};