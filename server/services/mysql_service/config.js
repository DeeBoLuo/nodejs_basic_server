module.exports = {
    "host": "localhost",
    "user": "root",
    "password": "root",
    "database": "black_red",
    "waitForConnections": true,
    "connectionLimit": 10,
    "queueLimit": 0
};