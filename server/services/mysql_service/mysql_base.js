const mysql = require('mysql2');

module.exports = class {
    constructor() {
        this.m_dbPool = null;
        this.m_poolPromise = null;
    }

    getDB() {
        if (!this.m_dbPool) {
            console.error('数据库未成功初始化');
        }
        return this.m_poolPromise;
    }

    initDB(config) {
        if (this.dbPool) return this.dbPool;
        //
        this.dbPool = mysql.createPool(config);
        this.m_poolPromise = this.dbPool.promise();
    }

    async query(sql, args) {
        console.debug('数据库执行:', sql, '参数:', args);
        let [rows, fields] = await this.m_poolPromise.query(sql, args);
        return Promise.resolve([rows, fields]);
    }

    getMysqlPromise() {
        return this.m_poolPromise;
    }
};