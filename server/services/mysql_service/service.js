const mysql = require('./mysql_base');
const mysql_config = require('./config');

module.exports = class {
    constructor() {
        //
        this.m_mysqlBase = new mysql();
        this.m_mysqlBase.initDB(mysql_config);
    }

    //测试
    async selectData() {
        return await this.m_mysqlBase.query(`select * from t_palace_list`);
    }
}