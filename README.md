# NodeJs基础服务端

## 介绍
- 这是一款NodeJs全异步的基础服务器

## 软件架构
- 整体代码均支持Promise、async 和 await 异步写法
- mysql服务 使用mysql2库，速度比mysql更快
- http服务 使用koa库 并封装了中间件和解析get、post请求参数，以及异步返回的封装
- log服务 使用log4js库 

## 安装教程

1. 下载库文件到根目录 执行命令 npm install
2. 修改对应服务配置文件(基本无需修改，除了mysql的配置)
3. 启动服务器 执行命令 npm run server
4. 启动成功后 可到浏览器访问[服务器主页](http://127.0.0.1:2012/home)查看是否正常开启(默认:http://127.0.0.1:2012/home)

